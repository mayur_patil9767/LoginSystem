<div class="container">
    <h2>Login to Your Account</h2>
    
    <!-- Status message -->
    <?php  
        if(!empty($success_msg)){ 
            echo '<p class="status-msg success">'.$success_msg.'</p>'; 
        }elseif(!empty($error_msg)){ 
            echo '<p class="status-msg error">'.$error_msg.'</p>'; 
        } 
    ?>
    
    <!-- Login form -->
    <div class="regisFrm">
        <form action="" method="post">
            <div class="form-group">
                <input type="email" name="email" id="email" placeholder="EMAIL" required="">
                <?php echo form_error('email','<p class="help-block">','</p>'); ?>
            </div>
            <div class="form-group" id="passdiv" style="display:none;">
                <input type="password" name="password" placeholder="PASSWORD" required="">
                <?php echo form_error('password','<p class="help-block">','</p>'); ?>
            </div>
            <div class="send-button" id="emailsub">
                <input type="button" name="emailSubmit" id="emailSubmit" value="SUBMIT">
            </div>
            <div class="send-button" id="loginsub" style="display: none;">
                <input type="submit" name="loginSubmit" value="LOGIN">
            </div>
        </form>
        <!-- <p>Don't have an account? <a href="<?php echo base_url('users/registration'); ?>">Register</a></p> -->
    </div>
</div>


<script type="text/javascript">
    
     $(document).ready(function(){

      $('#emailSubmit').click(function(){
       
         var emailid=$('#email').val();
         
        $.ajax({
            method: "POST",
            url: "<?=base_url()?>Users/email_validation",
            data: {email: emailid},
            dataType: 'json',
            success: function(result) { 
                if(result==1){
                  $('#passdiv').show(); 
                  $('#emailsub').hide(); 
                  $('#loginsub').show(); 
                }
                else
                {
                    alert("The account not found for given email"); 
                    window.location = "<?=base_url()?>Users/registration/?email="+ result;
                 
                }
                    
                    
            }
            
        });
       
          });



    });
</script>