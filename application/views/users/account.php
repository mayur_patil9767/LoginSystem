<div class="container">
	<h2>Dashboard</h2>
    <h2>Welcome <?php echo $user['first_name']; ?>!</h2>
    <a href="<?php echo base_url('users/logout'); ?>" class="logout">Logout</a>
    <div class="regisFrm">
        <p><b>Name: </b><?php echo $user['first_name'].' '.$user['last_name']; ?></p>
        <p><b>Email: </b><?php echo $user['email']; ?></p>
        <p><b>Phone: </b><?php echo $user['phone']; ?></p>
         <?php $filepath = base_url()."uploads/".$user['prof_pic'];?>
        <p><b>Profile Pic: </b><img src="<?php echo $filepath; ?>" alt="No Profile Pic Uploaded" class="img-responsive" width="100" height="100"></p>
        <form action="../updateuserdata" method="post" enctype="multipart/form-data">
        <p><b>Update Profile Picture: </b><input name="file" type="file" id="image_id"/></p>
        <p><b>Department: </b>
        					<select name="dept" id="dept">
        						
        						<option value="">Select Department</option>
        					 <?php
        					  foreach($dept as $dep){
    						 
        					  if(!empty($user['dept_id']) && ($user['dept_id']==$dep['dept_id'])){
        					 echo '<option value="'.$dep['dept_id'].'" selected>'.$dep['dept_name'].'</option>';
        					   
        					  }
        					 echo '<option value="'.$dep['dept_id'].'">'.$dep['dept_name'].'</option>';
        					   
        					  }
        					  ?>
        					</select>
    					</p>
        				<p><b>Sub Dep: </b><select name="subdept" id="subdept">
        					<option value="">Select Sub Department</option>
        					<?php
        					if(!empty($user['sub_dept'])){
        					 echo '<option value="'.$subdept['sub_id'].'" selected>'.$subdept['sub_dept'].'</option>';
        					   
        					  }
        					  ?>
        					</select>
        </p>
        <input type="hidden" name="userid" value="<?php echo $user['id'];?>"/>
        <div class="send-button">
                <input type="submit" name="updateprof" value="UPDATE ACCOUNT">
            </div>
    	</form>
    </div>
</div>

<script type="text/javascript">
    
     $(document).ready(function(){
      $('#dept').change(function(){
       
         var deptid=$('#dept').val();
        $.ajax({
            method: "POST",
            url: "<?=base_url()?>Users/sub_dept",
            data: {deptid: deptid},
            dataType: 'json',
            success: function(response) {
           		var len = response.length;
           		var opt_str="";
            	for(var i=0; i<len; i++){
                var id = response[i].sub_id;
                var sub_dept = response[i].sub_dept;
                opt_str += "<option value='"+id+"'>"+sub_dept+"</option>";
                }  
                $("#subdept").html(opt_str);      
            }
            
        });
       
          });



    });
</script>