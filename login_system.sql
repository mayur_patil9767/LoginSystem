-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.1.37-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win32
-- HeidiSQL Version:             10.2.0.5599
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table login_system.departments
CREATE TABLE IF NOT EXISTS `departments` (
  `dept_id` int(11) NOT NULL,
  `dept_name` varchar(20) NOT NULL,
  `description` text NOT NULL,
  PRIMARY KEY (`dept_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table login_system.departments: ~2 rows (approximately)
/*!40000 ALTER TABLE `departments` DISABLE KEYS */;
INSERT INTO `departments` (`dept_id`, `dept_name`, `description`) VALUES
	(1, 'IT', 'information technology'),
	(2, 'management', 'management');
/*!40000 ALTER TABLE `departments` ENABLE KEYS */;

-- Dumping structure for table login_system.sub_departments
CREATE TABLE IF NOT EXISTS `sub_departments` (
  `sub_id` int(11) NOT NULL,
  `dept_id` int(11) NOT NULL,
  `sub_dept` varchar(20) NOT NULL,
  `description` text NOT NULL,
  PRIMARY KEY (`sub_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table login_system.sub_departments: ~4 rows (approximately)
/*!40000 ALTER TABLE `sub_departments` DISABLE KEYS */;
INSERT INTO `sub_departments` (`sub_id`, `dept_id`, `sub_dept`, `description`) VALUES
	(1, 1, 'hardware', 'technologies hardware dept'),
	(2, 2, 'admin', 'administration'),
	(3, 2, 'lecturer', 'lecturers'),
	(4, 1, 'Software', 'soft department');
/*!40000 ALTER TABLE `sub_departments` ENABLE KEYS */;

-- Dumping structure for table login_system.users
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `last_name` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `gender` enum('Male','Female') COLLATE utf8_unicode_ci NOT NULL,
  `dept_id` int(11) NOT NULL,
  `sub_dept` int(11) NOT NULL,
  `prof_pic` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1=Active | 0=Inactive',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table login_system.users: ~2 rows (approximately)
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`, `first_name`, `last_name`, `email`, `password`, `gender`, `dept_id`, `sub_dept`, `prof_pic`, `phone`, `created`, `modified`, `status`) VALUES
	(7, 'mayur', 'patil', 'mayur.p@gmail.com', 'd936834d459daa634a4020b58783095a', 'Male', 2, 2, '7_id-card.png', '9112321212', '2021-05-15 09:59:37', '2021-05-15 09:59:37', 1);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
